package com.connquest.cqbleanalyzer

import android.annotation.SuppressLint
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.content.res.Resources
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.preference.PreferenceManager
import com.connquest.cqbleanalyzer.bluetooth.*
import com.connquest.cqbleanalyzer.error.CustomException
import com.connquest.cqbleanalyzer.error.ScanErrorCode
import com.connquest.cqbleanalyzer.location.LocationDisabledException
import com.connquest.cqbleanalyzer.location.LocationEnabler
import com.connquest.cqbleanalyzer.location.LocationResolver
import com.connquest.cqbleanalyzer.logger.LogExporter
import com.connquest.cqbleanalyzer.logger.LogMessage
import com.connquest.cqbleanalyzer.logger.MessagePriority
import com.connquest.cqbleanalyzer.permission.LocationPermissionChecker
import com.connquest.cqbleanalyzer.permission.LocationPermissionEnabler
import com.connquest.cqbleanalyzer.permission.LocationPermissionException
import com.connquest.cqbleanalyzer.permission.PermissionDialog
import com.connquest.cqbleanalyzer.scan.AutoscanHandler
import com.connquest.cqbleanalyzer.scan.ScanController
import com.connquest.cqbleanalyzer.scan.ScanItem
import com.connquest.cqbleanalyzer.utility.Request
import com.connquest.cqbleanalyzer.utility.Request.Companion.requestLocation
import com.connquest.cqbleanalyzer.view.*
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar

class ActivityHelper {
    companion object {
        var firstInit = true
    }
}

class MainActivity : AppCompatActivity(),
    BluetoothStateListener.EventListener, ScanController.ScanListener,
    ScanFragment.EventListener, ScanListAdapter.OnClickListener,
    LocationPermissionEnabler.EventListener, PermissionDialog.OnConfirmation,
    DeviceServiceDiscoveryController.EventListener, DeviceBonder.EventListener,
    DeviceDetailsFragment.EventListener, AboutPageFragment.EventListener,
    SharedPreferences.OnSharedPreferenceChangeListener, AppPreferences.EventListener,
    AutoscanHandler.EventListener, LogFragment.EventListener, LogExporter.EventListener,
    SplashFragment.EventListener {

    companion object {
        private var screenParams: ScreenParams? = null

        private var bluetoothResolver: BluetoothResolver? = null
        private var bluetoothStateListener: BluetoothStateListener? = null
        private var scanController: ScanController? = null
        private var autoscanHandler: AutoscanHandler? = null
        private var deviceServiceDiscoveryController: DeviceServiceDiscoveryController? = null
        private var deviceBonder: DeviceBonder? = null

        private var locationPermissionChecker: LocationPermissionChecker? = null
        private var locationPermissionEnabler: LocationPermissionEnabler? = null
        private var locationResolver: LocationResolver? = null
        private var locationEnabler: LocationEnabler? = null

        private var logExporter: LogExporter? = null
    }

    private lateinit var scanViewModel: ScanViewModel
    private lateinit var deviceDetailsViewModel: DeviceDetailsViewModel
    private lateinit var logViewModel: LogViewModel

    private lateinit var navigationController: NavController
    private lateinit var toolbar: Toolbar
    private lateinit var actionBar: ActionBar
    private lateinit var drawerToggle: ActionBarDrawerToggle
    private lateinit var navigationView: NavigationView
    private lateinit var drawerLayout: DrawerLayout

    private lateinit var sharedPreferences: SharedPreferences

    enum class BluetoothActionState {
        SCAN, BOND, CONNECT
    }

    private var bluetoothActionState: BluetoothActionState = BluetoothActionState.SCAN
    private var bluetoothItem = ScanItem()

    private var themeChange = false

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (screenParams == null) screenParams = ScreenParams(this)

        initViewModels()

        initBluetoothResolver()
        initBluetoothStateListener()
        initDeviceBonder()
        initDeviceServiceDiscoverer()
        initScanController()

        initAutoscanHandler()

        navigationController = Navigation.findNavController(this, R.id.nav_host_fragment)

        setupToolbar()
        setupDrawer()
        setupNavigationView()

        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        initPermissionChecker()
        initLocationPermissionEnabler()
        initLocationResolver()
        initLocationEnabler()
        initLogExporter()

        if (ActivityHelper.firstInit) setLogPreferences()
    }

    private fun initBluetoothResolver() {
        try {
            if (bluetoothResolver == null) {
                bluetoothResolver = BluetoothResolver(applicationContext)
                bluetoothResolver?.initializeBluetoothAdapter()
            }
        } catch (exception: BluetoothResolverException) {
            reportAdapterError(exception.message.toString())
        }
    }

    private fun initBluetoothStateListener() {
        if (bluetoothStateListener == null)  bluetoothStateListener = BluetoothStateListener(applicationContext)
        bluetoothStateListener?.setEventListener(this)
    }

    private fun initDeviceBonder() {
        if (deviceBonder == null) deviceBonder = DeviceBonder(applicationContext)
        deviceBonder?.setEventListener(this)
    }

    private fun initDeviceServiceDiscoverer() {
        if (deviceServiceDiscoveryController == null) deviceServiceDiscoveryController = DeviceServiceDiscoveryController(applicationContext)
        deviceServiceDiscoveryController?.setEventListener(this)
    }

    private fun initScanController() {
        if (scanController == null) {
            scanController = ScanController(this, bluetoothResolver?.bluetoothAdapter)
            val scanDuration = sharedPreferences.getInt(resources.getString(R.string.scanDurationKey), 10)
            val scanMode = sharedPreferences.getString(resources.getString(R.string.scanModeKey), "0")!!.toInt()
            scanController!!.setScanDuration(scanDuration)
            scanController!!.setScanMode(scanMode)
        } else {
            scanController?.setContext(this)
        }
    }

    private fun initAutoscanHandler() {
        if (autoscanHandler == null) {
            autoscanHandler = AutoscanHandler(this)

            val autoscanOn = sharedPreferences.getBoolean(resources.getString(R.string.autoscanKey), false)
            if (autoscanOn) {
                val autoscanInterval = sharedPreferences.getString(resources.getString(R.string.autoscanValueKey), "30")?.toInt()
                autoscanHandler?.enableAutoscanAndSetInterval(autoscanInterval!!)
            }
        } else {
            autoscanHandler?.setContext(this)
        }
    }

    private fun initViewModels() {
        scanViewModel = ViewModelProvider(this).get(ScanViewModel::class.java)
        deviceDetailsViewModel = ViewModelProvider(this).get(DeviceDetailsViewModel::class.java)
        logViewModel = ViewModelProvider(this).get(LogViewModel::class.java)
    }

    private fun setLogPreferences() {
        logViewModel.messagePriority =
            MessagePriority.getValueForOrdinal(
                sharedPreferences.getString(resources.getString(R.string.logPriorityKey), "0")!!.toInt())
        logViewModel.isTimestampVisible =
            sharedPreferences.getBoolean(resources.getString(R.string.timestampKey), true)
    }

    private fun initPermissionChecker() {
        if (locationPermissionChecker == null) locationPermissionChecker = LocationPermissionChecker(applicationContext)
    }

    private fun initLocationPermissionEnabler() {
        if (locationPermissionEnabler == null) locationPermissionEnabler = LocationPermissionEnabler(this, this)
        else locationPermissionEnabler!!.setupActivity(this, this)
    }

    private fun initLocationResolver() {
        if (locationResolver == null) locationResolver = LocationResolver(applicationContext)
    }

    private fun initLocationEnabler() {
        if (locationEnabler == null) locationEnabler = LocationEnabler(this)
        else locationEnabler!!.setActivity(this)
    }

    private fun initLogExporter() {
        if (logExporter == null) logExporter = LogExporter(this)
        else logExporter?.setContext(this)
    }

    private fun setupToolbar() {
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        actionBar = supportActionBar!!
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayShowHomeEnabled(true)
    }

    private fun setupDrawer() {
        drawerLayout = findViewById(R.id.drawerLayout)
        drawerToggle = object : ActionBarDrawerToggle(this, drawerLayout,
            R.string.drawer_open, R.string.drawer_close
        ) {
            override fun onDrawerStateChanged(newState: Int) {
                invalidateOptionsMenu()
            }
        }
        drawerLayout.addDrawerListener(drawerToggle)
    }

    private fun setupNavigationView() {
        navigationView = findViewById(R.id.navView)
        navigationView.setNavigationItemSelectedListener { menuItem: MenuItem ->
            when (menuItem.itemId) {
                R.id.log -> navigationController.navigate(R.id.openLog)
                R.id.preferences -> navigationController.navigate(R.id.openPreferences)
                R.id.drawerListAbout -> navigationController.navigate(R.id.openAboutPage)
                else -> return@setNavigationItemSelectedListener true
            }
            true
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        when (scanViewModel.isScanActive()) {
            true -> {
                menu?.findItem(R.id.action_scan)?.isVisible = false
                menu?.findItem(R.id.action_stop_scan)?.isVisible = true
            }
            false -> {
                menu?.findItem(R.id.action_stop_scan)?.isVisible = false
                menu?.findItem(R.id.action_scan)?.isVisible = true
            }
        }
        menu?.findItem(R.id.action_scan)?.isEnabled = !drawerLayout.isDrawerOpen(GravityCompat.START)
        menu?.findItem(R.id.action_stop_scan)?.isEnabled = !drawerLayout.isDrawerOpen(GravityCompat.START)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (isDrawerItemSelected(item)) return true

        when (item.itemId) {
            R.id.action_scan -> scanDevices()
            R.id.action_stop_scan -> scanController?.stopScanIfStarted()
        }

        return super.onOptionsItemSelected(item)
    }

    private fun isDrawerItemSelected(item: MenuItem): Boolean {
        return drawerToggle.isDrawerIndicatorEnabled && drawerToggle.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun scanFragmentCreated() {
        setupScanFragmentToolbar()

        if (ActivityHelper.firstInit) {
            checkAutoStartScan()
            ActivityHelper.firstInit = false
        }
    }

    private fun setupScanFragmentToolbar() {
        actionBar.title = resources.getString(R.string.app_name)
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.show()

        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        drawerToggle.isDrawerIndicatorEnabled = true
        drawerToggle.syncState()
    }

    private fun checkAutoStartScan() {
        if (sharedPreferences.getBoolean(resources.getString(R.string.autoStartScanKey), true)) scanDevices()
    }

    override fun autoscan() {
        try {
            checkIsScanAvailable()
            startScan()
            invalidateOptionsMenu()
        } catch (exception: CustomException) {}
    }

    override fun scanDevices() {
        try {
            checkIsScanAvailable()
            autoscanHandler?.resetHandler()
            startScan()
            invalidateOptionsMenu()
        } catch (exception: CustomException) {
            logViewModel.logMessage(LogMessage(MessagePriority.WARNING, System.currentTimeMillis(), exception.message.toString()))
            resolveScanException(exception)
        }
    }

    private fun checkIsScanAvailable() {
        scanViewModel.checkIsScanActive()
        bluetoothResolver?.checkBluetooth()
        locationPermissionChecker?.checkLocationPermission()
        locationResolver?.checkLocation()
    }

    private fun resolveScanException(exception: CustomException) {
        when (exception) {
            is BluetoothResolverException -> enableBluetoothForScan()
            is LocationDisabledException -> locationEnabler?.enableLocation()
            is LocationPermissionException -> locationPermissionEnabler?.requestLocationPermission()
            else -> {}
        }
    }

    private fun enableBluetoothForScan() {
        if (bluetoothResolver!!.isAdapterValid()) {
            enableBluetooth()
            bluetoothActionState = BluetoothActionState.SCAN
        } else {
            reportAdapterError("Bluetooth adapter is not valid.")
        }
    }

    private fun enableBluetooth() {
        startActivityForResult(Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), Request.enableBluetooth, null)
    }

    private fun reportAdapterError(message: String) {
        showMessage(message)
        logViewModel.logMessage(LogMessage(MessagePriority.ERROR, System.currentTimeMillis(), message))
    }

    private fun showMessage(message: String) {
        Snackbar.make(findViewById(R.id.main_activity), message, Snackbar.LENGTH_LONG).show()
    }

    private fun startScan() {
        scanController?.scanDevices()
        scanViewModel.scanStarted()
        logViewModel.logMessage(LogMessage(MessagePriority.INFO, System.currentTimeMillis(), "Scan started."))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode) {
            Request.enableLocation -> if (resultCodeOk(resultCode)) scanDevices()
            Request.enableBluetooth -> if (resultCodeOk(resultCode)) resolveBluetoothAction()
            Request.createFile -> if (resultCodeOk(resultCode)) logExporter?.writeToFile(data?.data)
        }
    }

    private fun resultCodeOk(resultCode: Int) = resultCode == Activity.RESULT_OK

    private fun resolveBluetoothAction() {
        when (bluetoothActionState) {
            BluetoothActionState.SCAN -> scanDevices()
            BluetoothActionState.BOND -> deviceBonder?.bond(bluetoothItem.device!!)
            BluetoothActionState.CONNECT -> showDetails(bluetoothItem)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            requestLocation -> if (permissionGranted(grantResults)) scanDevices()
        }
    }

    private fun permissionGranted(grantResults: IntArray): Boolean {
        return (grantResults.count() > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
    }

    override fun showPermissionRationale() {
        val locationDialog = PermissionDialog(this)

        val locationArgs = Bundle()
        locationArgs.putCharSequence("title", resources.getString(R.string.location_request_title))
        locationArgs.putCharSequence("message", resources.getString(R.string.location_request_message))
        locationArgs.putInt("requestCode", requestLocation)
        locationDialog.arguments = locationArgs

        locationDialog.show(supportFragmentManager, "locationAlert")
    }

    override fun onRationaleConfirmation(requestCode: Int?) {
        when (requestCode) {
            requestLocation -> {
                locationPermissionEnabler?.rationaleDenied = false
                locationPermissionEnabler?.requestLocationPermission()
                return
            }
        }
    }

    override fun onBluetoothDisabled() {
        scanController?.stopScanIfStarted()
        logViewModel.logMessage(LogMessage(MessagePriority.WARNING, System.currentTimeMillis(),
            "Bluetooth disabled."))
    }

    override fun onBluetoothEnabled() {
        scanController?.initializeLowEnergyScanner()
        if (autoscanHandler!!.isAutoscanOn) autoscanHandler?.startHandler()
        logViewModel.logMessage(LogMessage(MessagePriority.INFO, System.currentTimeMillis(),
            "Bluetooth enabled."))
    }

    override fun onScanResult(scanItem: ScanItem) {
        try {
            if (!scanController?.isScanRunning!!) return
            scanViewModel.receiveScanItem(scanItem)
            deviceDetailsViewModel.updateDeviceIfShown(scanItem)
            logViewModel.logMessage(LogMessage(MessagePriority.DEBUG, scanItem.timestamp,
                "Scan update (${scanItem.address})."))
        } catch (exception: Exception) {}
    }

    override fun onScanFinished() {
        scanViewModel.scanFinished()
        if (autoscanHandler!!.isAutoscanOn) autoscanHandler?.startHandler()
        logViewModel.logMessage(LogMessage(MessagePriority.INFO, System.currentTimeMillis(), "Scan completed."))
    }

    override fun onScanErrorReceived(scanErrorCode: ScanErrorCode) {
        onScanFinished()
        showMessage(scanErrorCode.toString())
        logViewModel.logMessage(LogMessage(MessagePriority.ERROR, System.currentTimeMillis(), scanErrorCode.toString()))
    }

    override fun showDetails(scanItem: ScanItem) {
        try {
            if (!deviceDetailsViewModel.isDeviceCached(scanItem)) {
                bluetoothResolver?.checkBluetooth()
                deviceServiceDiscoveryController?.discover(scanItem)
                logViewModel.logMessage(LogMessage(MessagePriority.INFO, System.currentTimeMillis(),
                "Connecting to ${scanItem.address}."))
            }
            deviceDetailsViewModel.setDevice(ScanItem(scanItem))
            navigationController.navigate(R.id.show_details)
        } catch (exception: BluetoothResolverException) {
            enableBluetoothForConnection(scanItem)
        } catch (exception: IllegalArgumentException) {}
    }

    private fun enableBluetoothForConnection(scanItem: ScanItem) {
        if (bluetoothResolver!!.isAdapterValid()) {
            enableBluetooth()
            bluetoothItem = ScanItem(scanItem)
            bluetoothActionState = BluetoothActionState.CONNECT
        } else {
            reportAdapterError("Bluetooth adapter is not valid.")
        }
    }

    override fun bond(scanItem: ScanItem) {
        try {
            bluetoothResolver?.checkBluetooth()
            deviceBonder?.bond(scanItem.device!!)
            logViewModel.logMessage(LogMessage(MessagePriority.INFO, System.currentTimeMillis(),
                "Pairing with ${scanItem.address}..."))
        } catch (exception: BluetoothResolverException) {
            enableBluetoothForPairing(scanItem)
        }
    }

    override fun bondDevice(scanItem: ScanItem?) {
        try {
            bluetoothResolver?.checkBluetooth()
            if (scanItem != null) {
                deviceBonder?.bond(scanItem.device!!)
                logViewModel.logMessage(LogMessage(MessagePriority.INFO, System.currentTimeMillis(),
                    "Pairing with ${scanItem.address}..."))
            }
        } catch (exception: BluetoothResolverException) {
            enableBluetoothForPairing(scanItem!!)
        }
    }

    private fun enableBluetoothForPairing(scanItem: ScanItem) {
        if (bluetoothResolver!!.isAdapterValid()) {
            enableBluetooth()
            bluetoothItem = scanItem
            bluetoothActionState = BluetoothActionState.BOND
        } else {
            reportAdapterError("Bluetooth adapter is not valid.")
        }
    }

    override fun bondStateChanged(device: BluetoothDevice) {
        scanViewModel.bondStateChanged(device)
        deviceDetailsViewModel.bondStateChanged(device)
        logViewModel.logMessage(LogMessage(MessagePriority.DEBUG, System.currentTimeMillis(),
            "Bond state changed: ${device.bondState}  (${device.address})."))
    }

    override fun servicesDiscovered(scanItem: ScanItem) {
        deviceDetailsViewModel.servicesDiscovered(scanItem)
        logViewModel.logMessage(LogMessage(MessagePriority.DEBUG, System.currentTimeMillis(),
            "Discovered services (${scanItem.address})."))
    }

    override fun detailsFragmentCreated(deviceName: String) {
        actionBar.title = deviceName
        disableDrawer()
    }

    override fun aboutPageCreated() {
        actionBar.title = resources.getString(R.string.about_text)
        disableDrawer()
    }

    override fun preferencesOpened() {
        actionBar.title = resources.getString(R.string.preferences)
        disableDrawer()
    }

    override fun logFragmentCreated() {
        actionBar.title = resources.getString(R.string.bluetooth_log)
        disableDrawer()
    }

    override fun exportLog(logMessages: List<LogMessage>) {
        logExporter?.export(logMessages)
    }

    override fun exportSuccessful() = showMessage("Log exported successfully.")

    override fun exportFailed() = showMessage("Export failed. Please try again!")

    private fun disableDrawer() {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        drawerToggle.isDrawerIndicatorEnabled = false
        drawerToggle.syncState()
    }

    override fun getTheme(): Resources.Theme? {
        val theme: Resources.Theme = super.getTheme()

        when (activeThemeString()) {
            "Dark" -> theme.applyStyle(R.style.AppThemeDark, true)
            "Light"  -> theme.applyStyle(R.style.AppThemeLight, true)
            else -> theme.applyStyle(R.style.AppThemeDark, true)
        }

        return theme
    }

    private fun activeThemeString() = sharedPreferences.getString(resources.getString(R.string.preferenceThemeKey), "Dark")

    override fun onSharedPreferenceChanged(p0: SharedPreferences?, p1: String?) {
        p0 ?: return
        p1 ?: return

        when (p1) {
            resources.getString(R.string.preferenceThemeKey) -> changeTheme()
            resources.getString(R.string.scanDurationKey) -> scanController?.setScanDuration(p0.getInt(p1, 10))
            resources.getString(R.string.scanModeKey) -> scanController?.setScanMode(p0.getString(p1, "0")!!.toInt())
            resources.getString(R.string.autoscanKey) -> {
                if (!p0.getBoolean(p1, false)) {
                    autoscanHandler?.disableAutoscan()
                } else {
                    val autoscanInterval = sharedPreferences.getString(resources.getString(R.string.autoscanValueKey), "30")?.toInt()
                    autoscanHandler?.enableAutoscanAndSetInterval(autoscanInterval!!)
                }
            }
            resources.getString(R.string.autoscanValueKey) -> {
                autoscanHandler?.enableAutoscanAndSetInterval(p0.getString(p1, "30")!!.toInt())
            }
            resources.getString(R.string.logPriorityKey) -> {
                logViewModel.messagePriority = MessagePriority.getValueForOrdinal(p0.getString(p1, "0")!!.toInt())
            }
            resources.getString(R.string.timestampKey) -> logViewModel.isTimestampVisible = p0.getBoolean(p1, true)
        }
    }

    override fun splashFragmentCreated() {
        actionBar.hide()
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        Handler().postDelayed(
            {
                navigationController.navigate(R.id.show_home)
            },
            3000L
        )
    }

    private fun changeTheme() {
        window.setWindowAnimations(R.style.ThemeTransitionAnimation)
        themeChange = true
        recreate()
    }

    override fun onResume() {
        super.onResume()
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this)
    }

    override fun onPause() {
        PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this)
        super.onPause()
    }

    override fun onDestroy() {
        bluetoothStateListener?.unregisterReceiver()
        deviceBonder?.unregisterReceiver()
        if (themeChange) {
            themeChange = false
        } else {
            scanController?.stopScanIfStarted()
            ActivityHelper.firstInit = true
        }
        super.onDestroy()
    }
}
