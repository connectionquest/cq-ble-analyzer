package com.connquest.cqbleanalyzer.location

import android.content.IntentSender
import androidx.appcompat.app.AppCompatActivity
import com.connquest.cqbleanalyzer.utility.Request
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsResponse
import com.google.android.gms.tasks.Task

class LocationEnabler(private var activity: AppCompatActivity) {
    fun setActivity(activity: AppCompatActivity) {
        this.activity = activity
    }

    fun enableLocation() {
        val builder = LocationSettingsRequest.Builder().addLocationRequest(LocationRequest.create())
            .setAlwaysShow(true)

        val settingsClient = LocationServices.getSettingsClient(activity)
        val checkLocationTask: Task<LocationSettingsResponse> = settingsClient.checkLocationSettings(builder.build())

        checkLocationTask.addOnFailureListener(activity) { e ->
            if (e is ResolvableApiException) showLocationDialog(e)
        }
    }

    private fun showLocationDialog(e: ResolvableApiException) {
        try {
            e.startResolutionForResult(activity, Request.enableLocation)
        } catch (sendEx: IntentSender.SendIntentException) {}
    }
}