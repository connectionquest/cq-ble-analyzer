package com.connquest.cqbleanalyzer.location

import android.content.Context
import android.location.LocationManager
import android.os.Build
import android.provider.Settings
import com.connquest.cqbleanalyzer.error.CustomException

class LocationDisabledException(message: String): CustomException(message)

class LocationResolver(private var context: Context) {
    private val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager

    fun checkLocation() {
        if(locationNotSupported() || isLocationDisabled()) throw LocationDisabledException("Location disabled.")
    }

    private fun locationNotSupported(): Boolean {
        val providers = locationManager.allProviders ?: return true
        return !providers.contains(LocationManager.GPS_PROVIDER)
    }

    private fun isLocationDisabled(): Boolean {
        return if (isAndroidPieOrHigher()) {
            !locationManager.isLocationEnabled
        } else {
            val locationMode = Settings.Secure.getInt(context.contentResolver, Settings.Secure.LOCATION_MODE)
            locationMode == Settings.Secure.LOCATION_MODE_OFF
        }
    }

    private fun isAndroidPieOrHigher() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.P
}