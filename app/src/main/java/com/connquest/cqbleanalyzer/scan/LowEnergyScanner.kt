package com.connquest.cqbleanalyzer.scan

import android.bluetooth.BluetoothAdapter
import android.bluetooth.le.*
import android.os.Handler
import android.util.Log
import com.connquest.cqbleanalyzer.error.ScanErrorCode

class LowEnergyScanner(private val scanListener: OnScanListener, private val bluetoothAdapter: BluetoothAdapter?) {
    var scanDuration: Long = 10000L

    private var lowEnergyScanner: BluetoothLeScanner? = null

    private val scanHandler = Handler()
    private var scanSettings = ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_POWER).build()

    interface OnScanListener {
        fun onDeviceFound(scanResult: ScanResult)
        fun onScanFailed(scanErrorCode: ScanErrorCode)
        fun onScanFinished()
    }

    fun initialize() {
        bluetoothAdapter ?: return

        if (lowEnergyScanner == null && bluetoothAdapter.isEnabled)
            lowEnergyScanner = bluetoothAdapter.bluetoothLeScanner
    }

    fun setScanMode(scanMode: Int) {
        scanSettings = ScanSettings.Builder().setScanMode(scanMode).build()
    }

    fun start() = startScanAndSetupStopHandler()

    private fun startScanAndSetupStopHandler() {
        lowEnergyScanner?.startScan(emptyList(), scanSettings, lowEnergyScanCallback)

        scanHandler.postDelayed({
            stopScanIfStarted()
        }, scanDuration)
    }

    fun stopScanIfStarted() {
        try {
            lowEnergyScanner?.stopScan(lowEnergyScanCallback)
        } catch (exception: IllegalStateException) {
            Log.d("LowEnergyScanner: ", exception.toString())
        } finally {
            scanHandler.removeCallbacksAndMessages(null)
            scanListener.onScanFinished()
        }
    }

    private val lowEnergyScanCallback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult) {
            scanListener.onDeviceFound(result)
        }

        override fun onBatchScanResults(results: MutableList<ScanResult>?) {
            results ?: return
            for (result in results) scanListener.onDeviceFound(result)
        }

        override fun onScanFailed(errorCode: Int) {
            scanListener.onScanFailed(ScanErrorCode.getValueForOrdinal(errorCode))
            stopScanIfStarted()
        }
    }

    init {
        initialize()
    }
}