package com.connquest.cqbleanalyzer.scan

import android.bluetooth.le.ScanResult
import android.os.Build
import android.text.format.DateFormat
import com.connquest.cqbleanalyzer.utility.DataConverter
import java.lang.NumberFormatException
import java.util.*

class ScanItemParser {
    fun getScanItemFromScanResult(scanResult: ScanResult) : ScanItem {
        val scanItem = ScanItem()

        if (scanResult.device != null) {
            scanItem.name = scanResult.device.name ?: ""
            scanItem.address = scanResult.device.address
            scanItem.bondState = scanResult.device.bondState
            scanItem.deviceType = scanResult.device.type

            scanItem.device = scanResult.device
        }

        scanItem.rssi = scanResult.rssi
        scanItem.txPower =  scanResult.scanRecord?.txPowerLevel ?: -100

        val timestamp = System.currentTimeMillis()
        scanItem.timestamp = timestamp

        val calendar = Calendar.getInstance()
        calendar.timeInMillis = timestamp
        scanItem.datetime = DateFormat.format("dd-MM-yyyy HH:mm:ss", calendar).toString()

        scanItem.rawData = parseRawData(DataConverter.byteArrayToHexString(scanResult.scanRecord?.bytes))

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            scanItem.isConnectable = scanResult.isConnectable
            scanItem.dataStatus = scanResult.dataStatus
            scanItem.primaryPhy = scanResult.primaryPhy
            scanItem.secondaryPhy = scanResult.secondaryPhy
        }

        return scanItem
    }

    private fun parseRawData(data: String): String {
        if (data.isEmpty()) return ""

        return try {
            var index = 0
            var type = data.take(2).toInt(16)

            while (type != 0) {
                index += type * 2 + 2
                type = data.slice(index until index + 2).toInt(16)
            }

            data.take(index)
        } catch (exception: NumberFormatException) {
            ""
        } catch (exception: IllegalArgumentException) {
            ""
        }
    }
}