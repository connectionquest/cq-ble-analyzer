package com.connquest.cqbleanalyzer.scan

import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattService

class ScanItem(scanItem: ScanItem? = null) {
    var name: String = ""
    var address: String = ""
    var bondState: Int = -1
    var isConnectable: Boolean = false
    var deviceType: Int = -1
    var batteryLevel = 0

    var rssi: Int = -128
    var txPower: Int = -100
    var advInterval = 0
    var datetime: String = ""
    var primaryPhy: Int = -1
    var secondaryPhy: Int = -1
    var dataStatus: Int = -1
    var rawData: String = ""

    var services: LinkedHashMap<BluetoothGattService, MutableList<BluetoothGattCharacteristic>> = linkedMapOf()

    var device: BluetoothDevice? = null
    var timestamp: Long = 0L
    var previousTimestamp: Long = 0L

    var rssiList: MutableList<Int> = mutableListOf()
    var advIntervalList: MutableList<Int> = mutableListOf()

    fun updateRssi(rssi: Int) {
        rssiList.add(rssi)
        this.rssi = rssiList.sum() / rssiList.size
    }

    fun updateAdvInterval(timestamp: Long) {
        if (previousTimestamp == 0L) {
            previousTimestamp = timestamp
            return
        }

        val interval = (timestamp - previousTimestamp).toInt()
        previousTimestamp = timestamp
        if (intervalOutOfOrder(interval)) return

        calculateAverageAdvInterval(interval)
    }

    private fun intervalOutOfOrder(interval: Int) = (interval > advInterval * 2 && advInterval > 0)

    private fun calculateAverageAdvInterval(interval: Int) {
        advIntervalList.add(interval)
        advInterval = advIntervalList.sum() / advIntervalList.size
    }

    init {
        if (scanItem != null) {
            name = scanItem.name
            address = scanItem.address
            bondState = scanItem.bondState
            isConnectable = scanItem.isConnectable
            deviceType = scanItem.deviceType
            batteryLevel = scanItem.batteryLevel

            rssi = scanItem.rssi
            txPower = scanItem.txPower
            advInterval = scanItem.advInterval
            datetime = scanItem.datetime
            primaryPhy = scanItem.primaryPhy
            secondaryPhy = scanItem.secondaryPhy
            dataStatus = scanItem.dataStatus
            rawData = scanItem.rawData

            services = LinkedHashMap(scanItem.services)

            device = scanItem.device
            timestamp = scanItem.timestamp
            previousTimestamp = scanItem.previousTimestamp

            rssiList = scanItem.rssiList.toMutableList()
            advIntervalList = scanItem.advIntervalList.toMutableList()
        }
    }
}