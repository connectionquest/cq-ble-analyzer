package com.connquest.cqbleanalyzer.scan

import android.bluetooth.BluetoothAdapter
import android.bluetooth.le.ScanResult
import com.connquest.cqbleanalyzer.error.ScanErrorCode

class ScanController(private var scanListener: ScanListener, bluetoothAdapter: BluetoothAdapter?)
    : LowEnergyScanner.OnScanListener {

    private val lowEnergyScanner = LowEnergyScanner(this, bluetoothAdapter)
    private val scanItemParser = ScanItemParser()

    var isScanRunning = false

    interface ScanListener {
        fun onScanResult(scanItem: ScanItem)
        fun onScanErrorReceived(scanErrorCode: ScanErrorCode)
        fun onScanFinished()
    }

    fun setContext(scanListener: ScanListener) {
        this.scanListener = scanListener
    }

    fun initializeLowEnergyScanner() = lowEnergyScanner.initialize()

    fun setScanDuration(duration: Int) {
        lowEnergyScanner.scanDuration = duration * 1000L
    }

    fun setScanMode(scanMode: Int) {
        if (scanMode < 0 || scanMode > 2) return
        lowEnergyScanner.setScanMode(scanMode)
    }

    fun scanDevices() {
        isScanRunning = true
        lowEnergyScanner.start()
    }

    fun stopScanIfStarted() {
        isScanRunning = false
        lowEnergyScanner.stopScanIfStarted()
    }

    override fun onDeviceFound(scanResult: ScanResult) {
        val scanItem = scanItemParser.getScanItemFromScanResult(scanResult)
        scanListener.onScanResult(scanItem)
    }

    override fun onScanFailed(scanErrorCode: ScanErrorCode) = scanListener.onScanErrorReceived(scanErrorCode)

    override fun onScanFinished() = scanListener.onScanFinished()
}