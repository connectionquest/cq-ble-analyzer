package com.connquest.cqbleanalyzer.scan

import android.content.Context
import android.os.Handler

class AutoscanHandler(val context: Context) {
    private var eventListener = context as EventListener

    private var scanIntervalInSeconds = 30
    var isAutoscanOn = false
    private var handler = Handler()

    interface EventListener {
        fun autoscan()
    }

    fun setContext(context: Context) {
        eventListener = context as EventListener
    }

    fun enableAutoscanAndSetInterval(interval: Int) {
        scanIntervalInSeconds = interval
        isAutoscanOn = true
        startHandler()
    }

    fun startHandler() {
        if (!isAutoscanOn) return

        resetHandler()

        handler.postDelayed({
            eventListener.autoscan()
        }, scanIntervalInSeconds * 1000.toLong())
    }

    fun resetHandler() {
        if (isAutoscanOn) handler.removeCallbacksAndMessages(null)
    }

    fun disableAutoscan() {
        resetHandler()
        isAutoscanOn = false
    }
}