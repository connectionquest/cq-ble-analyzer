package com.connquest.cqbleanalyzer.permission

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import com.connquest.cqbleanalyzer.error.CustomException

class LocationPermissionException(message: String): CustomException(message)

class LocationPermissionChecker(private var context: Context) {
    fun checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            throw LocationPermissionException("Location permission not granted.")
        }
    }
}