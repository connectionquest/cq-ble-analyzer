package com.connquest.cqbleanalyzer.permission

import android.app.AlertDialog
import android.app.Dialog
import android.content.res.Resources
import android.os.Bundle
import android.util.TypedValue
import androidx.annotation.StyleRes
import androidx.fragment.app.DialogFragment
import com.connquest.cqbleanalyzer.R

class PermissionDialog(private val onConfirmation: OnConfirmation) : DialogFragment() {
    interface OnConfirmation {
        fun onRationaleConfirmation(requestCode: Int?)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        @StyleRes val style = getDialogTheme()

        return AlertDialog.Builder(activity, style)
            .setTitle(arguments?.getCharSequence("title"))
            .setMessage(arguments?.getCharSequence("message"))
            .setNegativeButton(R.string.permission_dialog_negative, null)
            .setPositiveButton(R.string.permission_dialog_positive) { _, _: Int ->
                    onConfirmation.onRationaleConfirmation(arguments?.getInt("requestCode"))
            }.create()
    }

    private fun getDialogTheme(): Int {
        val theme: Resources.Theme = requireContext().theme
        val dialogStyle = TypedValue()
        theme.resolveAttribute(R.attr.alertDialogTheme, dialogStyle, true)
        return dialogStyle.data
    }
}