package com.connquest.cqbleanalyzer.permission

import android.Manifest
import android.app.Activity
import androidx.core.app.ActivityCompat
import com.connquest.cqbleanalyzer.utility.Request

class LocationPermissionEnabler(private var eventListener: EventListener, private var activity: Activity) {
    var rationaleDenied = true

    interface EventListener {
        fun showPermissionRationale()
    }

    fun setupActivity(eventListener: EventListener, activity: Activity) {
        this.eventListener = eventListener
        this.activity = activity
    }

    fun requestLocationPermission() {
        if (rationaleDenied && needToShowRationale()) eventListener.showPermissionRationale()
        else showPermissionDialog()
    }

    private fun showPermissionDialog() {
        ActivityCompat.requestPermissions(activity,
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
            Request.requestLocation
        )
    }

    private fun needToShowRationale(): Boolean {
        return ActivityCompat.shouldShowRequestPermissionRationale(activity,
            Manifest.permission.ACCESS_FINE_LOCATION)
    }
}