package com.connquest.cqbleanalyzer.bluetooth

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.Context

import com.connquest.cqbleanalyzer.error.CustomException

class BluetoothResolverException(message: String): CustomException(message)

class BluetoothResolver(private var context: Context) {
    var bluetoothAdapter: BluetoothAdapter? = null

    fun checkBluetooth() {
        if (bluetoothAdapter == null || !bluetoothAdapter!!.isEnabled) throw BluetoothResolverException("Bluetooth disabled.")
    }

    fun isAdapterValid() = bluetoothAdapter != null

    fun initializeBluetoothAdapter() {
        try {
            val bluetoothManager = context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
            bluetoothAdapter = bluetoothManager.adapter
        } catch (exception: NullPointerException) {
            throw BluetoothResolverException("Bluetooth adapter is not valid.")
        }
    }
}