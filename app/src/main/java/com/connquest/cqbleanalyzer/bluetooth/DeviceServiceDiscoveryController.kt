package com.connquest.cqbleanalyzer.bluetooth

import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattService
import android.content.Context
import com.connquest.cqbleanalyzer.scan.ScanItem

class DeviceServiceDiscoveryController(private val context: Context): DeviceServiceDiscoverer.EventListener {
    private lateinit var eventListener: EventListener

    private var scanItem: ScanItem? = null
    private val deviceServiceDiscoverer = DeviceServiceDiscoverer(this, context)

    interface EventListener {
        fun servicesDiscovered(scanItem: ScanItem)
    }

    fun setEventListener(eventListener: EventListener) {
        this.eventListener = eventListener
    }

    fun discover(scanItem: ScanItem) {
        scanItem.device ?: return
        this.scanItem = ScanItem(scanItem)
        deviceServiceDiscoverer.connectToDevice(this.scanItem!!.device!!)
    }

    override fun servicesDiscovered(services: LinkedHashMap<BluetoothGattService, MutableList<BluetoothGattCharacteristic>>, batteryLevel: Int) {
        scanItem ?: return
        scanItem!!.services = LinkedHashMap(services)
        scanItem!!.batteryLevel = batteryLevel
        eventListener.servicesDiscovered(scanItem!!)
    }
}