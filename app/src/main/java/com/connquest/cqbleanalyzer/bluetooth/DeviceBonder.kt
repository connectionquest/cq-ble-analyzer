package com.connquest.cqbleanalyzer.bluetooth

import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter

class DeviceBonder(private val context: Context): BroadcastReceiver() {
    private lateinit var eventListener: EventListener

    interface EventListener {
        fun bondStateChanged(device: BluetoothDevice)
    }

    fun setEventListener(eventListener: EventListener) {
        this.eventListener = eventListener
        registerReceiver()
    }

    fun bond(device: BluetoothDevice) {
        try {
            device.createBond()
        } catch (exception: NullPointerException) {}
    }

    fun unregisterReceiver() = context.unregisterReceiver(this)

    override fun onReceive(context: Context, intent: Intent) {
        val device = getDeviceFromIntent(intent)
        if (device != null) eventListener.bondStateChanged(device)
    }

    private fun registerReceiver() {
        val intentFilter = IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED)
        context.registerReceiver(this, intentFilter)
    }

    private fun getDeviceFromIntent(intent: Intent): BluetoothDevice? {
        return intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE)
    }
}