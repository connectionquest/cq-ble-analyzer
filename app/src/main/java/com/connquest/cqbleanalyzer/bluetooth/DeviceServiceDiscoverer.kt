package com.connquest.cqbleanalyzer.bluetooth

import android.bluetooth.*
import android.content.Context
import android.os.Build
import android.os.Handler
import androidx.annotation.RequiresApi

import com.connquest.cqbleanalyzer.scan.ScanItem
import java.lang.NullPointerException

class DeviceServiceDiscoverer(private val eventListener: EventListener, private val context: Context): BluetoothGattCallback() {
    private val batteryCharacteristicUuid = "00002a19-0000-1000-8000-00805f9b34fb"

    private val connectionDelayInMs = 200L
    private val autoConnect = false

    private var bluetoothGatt: BluetoothGatt? = null

    private var batteryCharacteristic: BluetoothGattCharacteristic? = null
    var services: LinkedHashMap<BluetoothGattService, MutableList<BluetoothGattCharacteristic>> = linkedMapOf()
    var batteryLevel = 0

    interface EventListener {
        fun servicesDiscovered(services: LinkedHashMap<BluetoothGattService, MutableList<BluetoothGattCharacteristic>>,
                               batteryLevel: Int)
    }

    fun connectToDevice(device: BluetoothDevice) {
        Handler().postDelayed({
            if (isAndroidMarshmallowOrHigher()) connectForMarshmallowOrHigher(device)
            else connectForLollipopOrLower(device)
        }, connectionDelayInMs)
    }

    private fun isAndroidMarshmallowOrHigher() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M

    @RequiresApi(Build.VERSION_CODES.M)
    private fun connectForMarshmallowOrHigher(device: BluetoothDevice) {
        bluetoothGatt = device.connectGatt(
            context, autoConnect,
            this, BluetoothDevice.TRANSPORT_LE
        )
    }

    private fun connectForLollipopOrLower(device: BluetoothDevice) {
        bluetoothGatt = device.connectGatt(
            context, autoConnect,
            this
        )
    }

    override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
        if(isOperationSuccessful(status) && isDeviceConnected(newState)) {
            gatt.discoverServices()
        }
    }

    override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
        if(isOperationSuccessful(status)) {
            readServicesAndCharacteristics()
            if (batteryCharacteristic != null) gatt.readCharacteristic(batteryCharacteristic)
            else reportServicesAndDisconnect()
        }
    }

    override fun onCharacteristicRead(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?, status: Int) {
        if (isOperationSuccessful(status)) readBatteryCharacteristic(characteristic)
        reportServicesAndDisconnect()
    }

    private fun isOperationSuccessful(status: Int) = status == BluetoothGatt.GATT_SUCCESS

    private fun isDeviceConnected(newState: Int) = newState == BluetoothProfile.STATE_CONNECTED

    private fun readServicesAndCharacteristics() {
        for (service in bluetoothGatt!!.services) {
            services[service] = mutableListOf()
            readCharacteristics(service)
        }
    }

    private fun readCharacteristics(service: BluetoothGattService) {
        for(characteristic in service.characteristics) {
            services[service]?.add(characteristic)
            if (isBatteryCharacteristic(characteristic)) batteryCharacteristic = characteristic
        }
    }

    private fun isBatteryCharacteristic(characteristic: BluetoothGattCharacteristic): Boolean {
        return characteristic.uuid.toString() == batteryCharacteristicUuid
    }

    private fun readBatteryCharacteristic(characteristic: BluetoothGattCharacteristic?) {
        characteristic ?: return
        val batteryLevel = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0)
        this.batteryLevel = batteryLevel
    }

    private fun reportServicesAndDisconnect() {
        eventListener.servicesDiscovered(services, batteryLevel)
        disconnectAndResetBatteryCharacteristic()
    }

    private fun disconnectAndResetBatteryCharacteristic() {
        bluetoothGatt?.disconnect()
        bluetoothGatt?.close()
        bluetoothGatt = null
        batteryCharacteristic = null
        services.clear()
        batteryLevel = 0
    }
}