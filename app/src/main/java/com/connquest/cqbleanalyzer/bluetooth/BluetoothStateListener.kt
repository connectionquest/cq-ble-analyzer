package com.connquest.cqbleanalyzer.bluetooth

import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter

class BluetoothStateListener(private var context: Context): BroadcastReceiver() {
    private lateinit var eventListener: EventListener

    interface EventListener {
        fun onBluetoothDisabled()
        fun onBluetoothEnabled()
    }

    fun setEventListener(eventListener: EventListener) {
        this.eventListener = eventListener
        registerReceiver()
    }

    fun unregisterReceiver() {
        context.unregisterReceiver(this)
    }

    private fun registerReceiver() {
        val intentFilter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        context.registerReceiver(this, intentFilter)
    }

    override fun onReceive(context: Context, intent: Intent) {
        val bluetoothState = getBluetoothStateFromIntent(intent)
        resolveBluetoothState(bluetoothState)
    }

    private fun getBluetoothStateFromIntent(intent: Intent): Int {
        return intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)
    }

    private fun resolveBluetoothState(bluetoothState: Int) {
        when (bluetoothState) {
            BluetoothAdapter.STATE_OFF -> eventListener.onBluetoothDisabled()
            BluetoothAdapter.STATE_ON -> eventListener.onBluetoothEnabled()
        }
    }
}