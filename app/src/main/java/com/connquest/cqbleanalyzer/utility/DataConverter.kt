package com.connquest.cqbleanalyzer.utility

class DataConverter {
    companion object {
        private val hexArray =
            charArrayOf('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f')

        fun byteArrayToHexString(bytes: ByteArray?): String {
            bytes ?: return ""

            val hexChars = CharArray(bytes.count() * 2)
            var value: Int
            for (index in bytes.indices) {
                value = (bytes[index].toInt() and 0xFF)
                hexChars[index * 2] = hexArray[value ushr 4]
                hexChars[index * 2 + 1] = hexArray[value and 0x0F]
            }
            return String(hexChars)
        }
    }
}