package com.connquest.cqbleanalyzer.utility

class Request {
    companion object {
        const val enableBluetooth = 1
        const val enableLocation = 2
        const val requestLocation = 3
        const val createFile = 4
    }
}