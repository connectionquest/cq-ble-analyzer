package com.connquest.cqbleanalyzer.error

enum class ScanErrorCode() {
    ScanFailedAlreadyStarted,
    ScanFailedApplicationRegistrationFailed,
    ScanFailedInternalError,
    ScanFailedFeatureUnsupported;

    companion object {
        fun getValueForOrdinal(ordinal: Int): ScanErrorCode = values()[ordinal - 1]
    }
}