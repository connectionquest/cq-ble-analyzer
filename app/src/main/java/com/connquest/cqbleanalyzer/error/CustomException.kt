package com.connquest.cqbleanalyzer.error

open class CustomException(message: String): Exception(message) {}