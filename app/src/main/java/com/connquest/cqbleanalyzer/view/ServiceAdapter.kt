package com.connquest.cqbleanalyzer.view

import android.annotation.SuppressLint
import android.app.Activity
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattService
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.TextView
import com.connquest.cqbleanalyzer.R
import com.connquest.cqbleanalyzer.scan.ScanItem
import java.util.*

class ServiceAdapter(activity: Activity): BaseExpandableListAdapter() {
    private val layoutInflater = activity.layoutInflater
    private var scanItem: ScanItem = ScanItem()

    override fun getChild(p0: Int, p1: Int): Any {
        return scanItem.services.values.toList()[p0][p1]
    }

    override fun getChildId(p0: Int, p1: Int): Long {
        return 0
    }

    @SuppressLint("InflateParams")
    override fun getChildView(p0: Int, p1: Int, p2: Boolean, p3: View?, p4: ViewGroup?): View {
        val characteristic = getCharacteristicString(p0, p1)

        var convertView = p3
        if (convertView == null) convertView = layoutInflater.inflate(R.layout.characteristic_layout, null)
        setCharacteristic(convertView!!, characteristic)

        return convertView
    }

    private fun getCharacteristicString(p0: Int, p1: Int): String {
        return (getChild(p0, p1) as BluetoothGattCharacteristic).uuid.toString().slice(4..7).toUpperCase(Locale.ROOT)
    }

    private fun setCharacteristic(view: View, characteristic: String) {
        val textView: TextView = view.findViewById(R.id.characteristic)
        textView.text = characteristic
    }

    override fun getChildrenCount(p0: Int): Int {
        return scanItem.services.values.toList()[p0].count()
    }

    override fun getGroup(p0: Int): Any {
        return scanItem.services.keys.toList()[p0]
    }

    override fun getGroupCount(): Int {
        return scanItem.services.keys.toList().count()
    }

    override fun getGroupId(p0: Int): Long {
        return 0
    }

    @SuppressLint("InflateParams")
    override fun getGroupView(p0: Int, p1: Boolean, p2: View?, p3: ViewGroup?): View {
        val service = getServiceString(p0)

        var convertView = p2
        if (convertView == null) convertView = layoutInflater.inflate(R.layout.service_layout, null)
        setService(convertView!!, service)

        return convertView
    }

    private fun getServiceString(p0: Int): String {
        return (getGroup(p0) as BluetoothGattService).uuid.toString().slice(4..7).toUpperCase(Locale.ROOT)
    }

    private fun setService(view: View, service: String) {
        val textView: TextView = view.findViewById(R.id.service)
        textView.text = service
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun isChildSelectable(p0: Int, p1: Int): Boolean {
        return false
    }

    fun setItem(scanItem: ScanItem) {
        if (scanItem.address.isNotEmpty()) {
            this.scanItem = scanItem
            notifyDataSetChanged()
        }
    }
}