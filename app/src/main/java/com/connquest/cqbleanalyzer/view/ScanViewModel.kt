package com.connquest.cqbleanalyzer.view

import android.bluetooth.BluetoothDevice
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.connquest.cqbleanalyzer.error.CustomException
import com.connquest.cqbleanalyzer.scan.ScanItem
import java.util.*

class ScanViewModel : ViewModel() {
    private val liveScanItems =  MutableLiveData<ArrayList<ScanItem>>()
    private val refreshingList = MutableLiveData<Boolean>()

    private val scanItems = ArrayList<ScanItem>()
    var updatedIndex = 0

    init {
        liveScanItems.value = scanItems
        refreshingList.value = false
    }

    fun scanItems(): LiveData<ArrayList<ScanItem>> = liveScanItems

    fun refreshingList(): LiveData<Boolean> = refreshingList

    fun receiveScanItem(scanItem: ScanItem) {
        if (scanItem.address.isEmpty()) return
        val itemIndex = indexOfItem(scanItem.address)
        if (itemExists(itemIndex)) {
            updateItem(scanItem, itemIndex)
            postChanges(itemIndex)
        } else {
            addItem(scanItem)
            postChanges(scanItems.count() - 1)
        }
    }

    private fun indexOfItem(address: String): Int {
        for (i in scanItems.indices) {
            if (scanItems[i].address == address) return i
        }
        return -1
    }

    private fun itemExists(index: Int) = index > -1

    private fun updateItem(scanItem: ScanItem, index: Int) {
        scanItems[index].name = scanItem.name
        scanItems[index].updateRssi(scanItem.rssi)
        scanItems[index].txPower = scanItem.txPower
        scanItems[index].isConnectable = scanItem.isConnectable
        scanItems[index].bondState = scanItem.bondState
        scanItems[index].datetime = scanItem.datetime
        scanItems[index].updateAdvInterval(scanItem.timestamp)
    }

    private fun postChanges(index: Int) {
        updatedIndex = index
        liveScanItems.value = scanItems
    }

    private fun addItem(scanItem: ScanItem) = scanItems.add(scanItem)

    fun scanStarted() {
        clear()
        refreshingList.value = true
    }

    fun isScanActive() = refreshingList.value!!

    fun checkIsScanActive() {
        if (refreshingList.value!!) throw CustomException("BLE scan already running")
    }

    fun scanFinished() {
        refreshingList.value = false
    }

    fun bondStateChanged(device: BluetoothDevice) {
        val itemIndex = indexOfItem(device.address)
        if (itemExists(itemIndex)) {
            scanItems[itemIndex].bondState = device.bondState
            postChanges(itemIndex)
        }
    }

    private fun clear() {
        scanItems.clear()
        updatedIndex = 0
        liveScanItems.value = scanItems
    }
}