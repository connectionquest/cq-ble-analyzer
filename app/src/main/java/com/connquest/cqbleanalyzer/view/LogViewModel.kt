package com.connquest.cqbleanalyzer.view

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.connquest.cqbleanalyzer.logger.LogMessage
import com.connquest.cqbleanalyzer.logger.MessagePriority

class LogViewModel: ViewModel() {
    private val liveLogMessages = MutableLiveData<MutableList<LogMessage>>()
    private val logMessages: MutableList<LogMessage> = mutableListOf()

    var messagePriority: MessagePriority = MessagePriority.INFO
    var isTimestampVisible = true

    init {
        liveLogMessages.value = logMessages
    }

    fun logMessages(): LiveData<MutableList<LogMessage>> = liveLogMessages

    fun logMessage(logMessage: LogMessage) {
        logMessages.add(logMessage)
        liveLogMessages.postValue(logMessages)
    }

    fun clearLog() {
        logMessages.clear()
        liveLogMessages.value = logMessages
    }
}