package com.connquest.cqbleanalyzer.view

import android.content.Context
import android.content.res.Resources
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.Transformation
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.connquest.cqbleanalyzer.R


class ScanFragment : Fragment() {
    private var gridColumns = 1

    private lateinit var viewModel: ScanViewModel
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var pullToRefreshTextView: TextView
    private lateinit var noDevicesNearbyTextView: TextView
    private lateinit var eventListener: EventListener
    private lateinit var scanItemsListView: RecyclerView
    private lateinit var adapter: ScanListAdapter

    private lateinit var pullToRefreshParams: ViewGroup.MarginLayoutParams
    private lateinit var scanListParams: ViewGroup.MarginLayoutParams

    interface EventListener {
        fun scanFragmentCreated()
        fun scanDevices()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        eventListener = context as EventListener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.scan_items_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        eventListener.scanFragmentCreated()

        adapter = ScanListAdapter(requireContext())
        viewModel = ViewModelProvider(requireActivity()).get(ScanViewModel::class.java)
        setObservers()

        setupScanList()
        setupInfoTexts()
        setupSwipeLayout()
    }

    private fun setObservers() {
        setScanItemObserver()
        setRefreshObserver()
    }

    private fun setScanItemObserver() {
        viewModel.scanItems().observe(viewLifecycleOwner, Observer { items ->
            adapter.setScanItems(items, viewModel.updatedIndex)})
    }

    private fun setRefreshObserver() {
        viewModel.refreshingList().observe(viewLifecycleOwner, Observer { isRefreshing : Boolean ->
            swipeRefreshLayout.isRefreshing = isRefreshing

            if (isRefreshing) listRefreshing()
            else listNotRefreshing()

            requireActivity().invalidateOptionsMenu()
        })
    }

    private fun listRefreshing() {
        pullToRefreshTextView.visibility = View.GONE
        noDevicesNearbyTextView.visibility = View.GONE

        if (scanListParams.topMargin == 0) setListTopMargin(112)
        else animateListTopMargin(112)
    }

    private fun listNotRefreshing() {
        pullToRefreshTextView.visibility = View.VISIBLE

        if (scanListParams.topMargin == 0) setListTopMargin(56)
        else animateListTopMargin(56)

        if (listEmpty()) {
            setPullToRefreshTopMargin(32)
            noDevicesNearbyTextView.visibility = View.VISIBLE
        } else {
            setPullToRefreshTopMargin(16)
            noDevicesNearbyTextView.visibility = View.GONE
        }
    }

    private fun listEmpty() = viewModel.scanItems().value!!.isEmpty()

    private fun setListTopMargin(margin: Int) {
        scanListParams.topMargin = margin * ScreenParams.dpRatio
    }

    private fun animateListTopMargin(finalMargin: Int) {
        val startMargin = scanListParams.topMargin
        val endMargin = finalMargin * ScreenParams.dpRatio
        val animation = object: Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
                scanListParams.topMargin = startMargin + ((endMargin - startMargin) * interpolatedTime).toInt()
                scanItemsListView.layoutParams = scanListParams
            }
        }
        animation.duration = 500
        scanItemsListView.startAnimation(animation)
    }

    private fun setPullToRefreshTopMargin(margin: Int) {
        pullToRefreshParams.topMargin = margin * ScreenParams.dpRatio
    }

    private fun setupScanList() {
        gridColumns = if (ScreenParams.isTablet) 2 else 1
        scanItemsListView = requireActivity().findViewById(R.id.scanItemsListView)
        scanItemsListView.layoutManager = GridLayoutManager(activity, gridColumns)
        scanItemsListView.itemAnimator = DefaultItemAnimator()
        scanItemsListView.adapter = adapter
        (scanItemsListView.itemAnimator as DefaultItemAnimator).supportsChangeAnimations = false
        scanItemsListView.setHasFixedSize(true)
        scanListParams = scanItemsListView.layoutParams as ViewGroup.MarginLayoutParams
    }

    private fun setupInfoTexts() {
        pullToRefreshTextView = requireActivity().findViewById(R.id.pullToRefreshText)
        noDevicesNearbyTextView = requireActivity().findViewById(R.id.noDevicesNearbyText)
        pullToRefreshParams = pullToRefreshTextView.layoutParams as ViewGroup.MarginLayoutParams
    }

    private fun setupSwipeLayout() {
        @ColorInt val indicatorColor = getIndicatorColor()
        @ColorInt val backgroundColor = getBackgroundColor()

        swipeRefreshLayout = requireActivity().findViewById(R.id.swipeRefreshLayout)
        swipeRefreshLayout.setColorSchemeColors(indicatorColor)
        swipeRefreshLayout.setProgressBackgroundColorSchemeColor(backgroundColor)
        swipeRefreshLayout.setOnRefreshListener {
            if (!viewModel.refreshingList().value!!) swipeRefreshLayout.isRefreshing = false
            eventListener.scanDevices()
        }
    }

    private fun getIndicatorColor(): Int {
        val theme: Resources.Theme = requireContext().theme
        val typedIndicatorColor = TypedValue()
        theme.resolveAttribute(R.attr.secondaryColor, typedIndicatorColor, true)
        return typedIndicatorColor.data
    }

    private fun getBackgroundColor(): Int {
        val theme: Resources.Theme = requireContext().theme
        val typedBackgroundColor = TypedValue()
        theme.resolveAttribute(R.attr.pullToRefreshBackground, typedBackgroundColor, true)
        return typedBackgroundColor.data
    }

    override fun onDestroyView() {
        scanItemsListView.adapter = null
        super.onDestroyView()
    }
}