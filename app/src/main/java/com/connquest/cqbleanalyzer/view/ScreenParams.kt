package com.connquest.cqbleanalyzer.view

import android.content.Context
import kotlin.math.pow
import kotlin.math.sqrt


class ScreenParams(private val context: Context) {
    companion object {
        var dpRatio: Int = 0
        var isTablet = false
    }

    init {
        dpRatio = context.resources.displayMetrics.density.toInt()
        isTablet = isTablet()
    }

    private fun isTablet(): Boolean {
        val metrics = context.resources.displayMetrics
        val widthInches = metrics.widthPixels / metrics.xdpi
        val heightInches = metrics.heightPixels / metrics.ydpi
        val diagonalInches = sqrt(widthInches.toDouble().pow(2.0) + heightInches.toDouble().pow(2.0))
        return diagonalInches >= 7.0
    }
}