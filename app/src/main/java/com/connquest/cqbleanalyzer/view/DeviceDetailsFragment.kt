package com.connquest.cqbleanalyzer.view

import android.annotation.SuppressLint
import android.bluetooth.BluetoothDevice
import android.bluetooth.le.ScanResult
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.DisplayMetrics
import android.view.*
import android.widget.ExpandableListView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.connquest.cqbleanalyzer.R
import com.connquest.cqbleanalyzer.scan.ScanItem


class DeviceDetailsFragment : Fragment() {
    private lateinit var viewModel: DeviceDetailsViewModel
    private lateinit var eventListener: EventListener

    private lateinit var deviceAddress: TextView
    private lateinit var bondState: TextView
    private lateinit var isConnectable: TextView
    private lateinit var deviceType: TextView
    private lateinit var batteryLevel: TextView

    private lateinit var rssi: TextView
    private lateinit var txPower: TextView
    private lateinit var advInterval: TextView
    private lateinit var timestamp: TextView
    private lateinit var primaryPhy: TextView
    private lateinit var secondaryPhy: TextView
    private lateinit var isTruncated: TextView
    private lateinit var rawData: TextView

    private lateinit var serviceMissingTextView: TextView
    private lateinit var serviceList: ExpandableListView
    private lateinit var serviceListAdapter: ServiceAdapter

    private var scanItem: ScanItem = ScanItem()

    private lateinit var serviceTimeoutHandler: Handler

    private var menu: Menu? = null

    interface EventListener {
        fun detailsFragmentCreated(deviceName: String)
        fun bondDevice(scanItem: ScanItem?)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.details_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        this.menu = menu
        menu.findItem(R.id.action_bond).isVisible =
            when (scanItem.bondState) {
                BluetoothDevice.BOND_BONDED, BluetoothDevice.BOND_BONDING -> false
                else -> true
            }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_bond -> eventListener.bondDevice(scanItem)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        eventListener = context as EventListener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.device_details, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProvider(requireActivity()).get(DeviceDetailsViewModel::class.java)
        eventListener.detailsFragmentCreated(viewModel.getDeviceName())
        serviceTimeoutHandler = Handler()

        referenceViews()

        serviceListAdapter = ServiceAdapter(requireActivity())
        serviceList.setAdapter(serviceListAdapter)
        setListIndicatorPosition()

        viewModel.device().observe(viewLifecycleOwner, Observer { device: ScanItem ->
            updateDeviceDetails(device)
        })
    }

    private fun referenceViews() {
        deviceAddress = requireActivity().findViewById(R.id.generalAddress)
        bondState = requireActivity().findViewById(R.id.generalBondState)
        isConnectable = requireActivity().findViewById(R.id.generalConnectable)
        deviceType = requireActivity().findViewById(R.id.generalDeviceType)
        batteryLevel = requireActivity().findViewById(R.id.generalBattery)
        rssi = requireActivity().findViewById(R.id.advRssi)
        txPower = requireActivity().findViewById(R.id.advTxPower)
        advInterval = requireActivity().findViewById(R.id.advIntervalDetails)
        timestamp = requireActivity().findViewById(R.id.advTimestamp)
        primaryPhy = requireActivity().findViewById(R.id.advPrimaryPhy)
        secondaryPhy = requireActivity().findViewById(R.id.advSecondaryPhy)
        isTruncated = requireActivity().findViewById(R.id.advTruncated)
        rawData = requireActivity().findViewById(R.id.advRaw)
        serviceList = requireActivity().findViewById(R.id.serviceList)
        serviceMissingTextView = requireActivity().findViewById(R.id.serviceMissingText)
    }

    private fun setListIndicatorPosition() {
        val width = getScreenWidthInPixels()
        serviceList.setIndicatorBoundsRelative(width - getPixelFromDips(30), width - getPixelFromDips(10))
    }

    private fun getScreenWidthInPixels(): Int {
        val metrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(metrics)
        return metrics.widthPixels
    }

    private fun getPixelFromDips(pixels: Int): Int {
        val scale = resources.displayMetrics.density
        return (pixels * scale + 0.5f).toInt()
    }

    @SuppressLint("SetTextI18n")
    private fun updateDeviceDetails(item: ScanItem) {
        scanItem = ScanItem(item)

        updateDeviceAddress()
        updateConnectableStatus()
        updateBondState()
        updateDeviceType()
        updateRssi()
        updateTxPower()
        updateAdvInterval()
        updateTimestamp()
        updateRawData()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) parseForAndroidOreo()
        else setUnknownValues()

        updateServices()
        updateBatteryLevel()
    }

    private fun updateDeviceAddress() {
        deviceAddress.text = if (scanItem.address.isEmpty()) "Unknown" else scanItem.address
    }

    private fun updateConnectableStatus() {
        try {
            isConnectable.text = if (scanItem.isConnectable) requireContext().resources.getString(R.string.yes)
                                 else requireContext().resources.getString(R.string.no)
        } catch (exception: NullPointerException) {
            isConnectable.text = requireContext().resources.getString(R.string.unknown)
        }
    }

    private fun updateBondState() {
        when(scanItem.bondState) {
            BluetoothDevice.BOND_BONDED -> {
                updateBondIconVisibility(false)
                bondState.text = requireContext().resources.getString(R.string.bonded_text)
            }
            BluetoothDevice.BOND_BONDING -> {
                updateBondIconVisibility(false)
                bondState.text = requireContext().resources.getString(R.string.bonding_text)
            }
            else -> {
                updateBondIconVisibility(true)
                bondState.text = requireContext().resources.getString(R.string.not_bonded_text)
            }
        }
    }

    private fun updateBondIconVisibility(isVisible: Boolean) {
        menu?.findItem(R.id.action_bond)?.isVisible = isVisible
        requireActivity().invalidateOptionsMenu()
    }

    private fun updateDeviceType() {
        when(scanItem.deviceType) {
            BluetoothDevice.DEVICE_TYPE_CLASSIC -> {
                deviceType.text = requireContext().resources.getString(R.string.device_type_classic)
            }
            BluetoothDevice.DEVICE_TYPE_LE -> {
                deviceType.text = requireContext().resources.getString(R.string.device_type_le)
            }
            BluetoothDevice.DEVICE_TYPE_DUAL -> {
                deviceType.text = requireContext().resources.getString(R.string.device_type_dual)
            }
            BluetoothDevice.DEVICE_TYPE_UNKNOWN -> {
                deviceType.text = requireContext().resources.getString(R.string.device_type_unknown)
            }
            else -> deviceType.text = requireContext().resources.getString(R.string.device_type_unknown)
        }
    }

    private fun updateRssi() {
        rssi.text = if (scanItem.rssi > -128) "${scanItem.rssi} dBm" else "-"
    }

    private fun updateTxPower() {
        txPower.text =
            if (scanItem.txPower > -100) scanItem.txPower.toString() + " dBm"
            else "< -100 dBm"
    }

    private fun updateAdvInterval() {
        advInterval.text = if (scanItem.advInterval == 0) "Unknown" else scanItem.advInterval.toString() + " ms"
    }

    private fun updateTimestamp() {
        timestamp.text = if (scanItem.datetime.isEmpty()) "Unknown" else scanItem.datetime
    }

    private fun updateRawData() {
        rawData.text = if (scanItem.rawData.isEmpty()) "Unknown" else scanItem.rawData
    }

    private fun parseForAndroidOreo() {
        updateDataStatus()
        updatePrimaryPhy()
        updateSecondaryPhy()
    }

    private fun updateDataStatus() {
        when(scanItem.dataStatus) {
            ScanResult.DATA_COMPLETE -> isTruncated.text = resources.getString(R.string.no)
            ScanResult.DATA_TRUNCATED -> isTruncated.text = resources.getString(R.string.yes)
            else -> isTruncated.text = resources.getString(R.string.unknown)
        }
    }

    private fun updatePrimaryPhy() {
        when(scanItem.primaryPhy) {
            BluetoothDevice.PHY_LE_1M -> primaryPhy.text = resources.getString(R.string.phy_le_1m)
            BluetoothDevice.PHY_LE_CODED -> primaryPhy.text = resources.getString(R.string.phy_le_coded)
            else -> primaryPhy.text = resources.getString(R.string.unknown)
        }
    }

    private fun updateSecondaryPhy() {
        when(scanItem.secondaryPhy) {
            BluetoothDevice.PHY_LE_1M -> secondaryPhy.text = resources.getString(R.string.phy_le_1m)
            BluetoothDevice.PHY_LE_CODED -> secondaryPhy.text = resources.getString(R.string.phy_le_coded)
            BluetoothDevice.PHY_LE_2M -> secondaryPhy.text = resources.getString(R.string.phy_le_2m)
            ScanResult.PHY_UNUSED -> secondaryPhy.text = resources.getString(R.string.phy_le_unused)
            else -> secondaryPhy.text = resources.getString(R.string.unknown)
        }
    }

    private fun setUnknownValues() {
        primaryPhy.text = resources.getString(R.string.unknown)
        secondaryPhy.text = resources.getString(R.string.unknown)
        isTruncated.text = resources.getString(R.string.unknown)
    }

    private fun updateServices() {
        if (scanItem.services.isEmpty()) {
            if (serviceMissingTextView.text.isBlank()) {
                serviceMissingTextView.text = resources.getString(R.string.searching_text)
                serviceTimeoutHandler
                    .postDelayed({
                        serviceMissingTextView.text = resources.getString(R.string.services_not_found)
                    },10000)
            }
        } else {
            serviceTimeoutHandler.removeCallbacksAndMessages(null)
            serviceListAdapter.setItem(scanItem)
            serviceMissingTextView.visibility = View.GONE
        }
    }

    private fun updateBatteryLevel() {
        batteryLevel.text = if (scanItem.batteryLevel > 0) scanItem.batteryLevel.toString() else "Unknown"
    }

    override fun onDestroy() {
        serviceTimeoutHandler.removeCallbacksAndMessages(null)
        super.onDestroy()
    }
}