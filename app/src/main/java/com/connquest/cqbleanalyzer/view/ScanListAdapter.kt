package com.connquest.cqbleanalyzer.view

import android.bluetooth.BluetoothDevice
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.connquest.cqbleanalyzer.R
import com.connquest.cqbleanalyzer.scan.ScanItem
import java.util.*


class ScanListAdapter(private val context: Context)
    : RecyclerView.Adapter<ScanListAdapter.ViewHolder>() {

    private val onClickListener = context as OnClickListener
    private var scanItems: MutableList<ScanItem> = mutableListOf()

    interface OnClickListener {
        fun showDetails(scanItem: ScanItem)
        fun bond(scanItem: ScanItem)
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        private val deviceName: TextView = view.findViewById(R.id.deviceName)
        private val deviceAddress: TextView = view.findViewById(R.id.deviceAddress)
        private val bondState: TextView = view.findViewById(R.id.bondStateContents)
        private val isConnectable: TextView = view.findViewById(R.id.isConnectable)
        private val rssi: TextView = view.findViewById(R.id.rssiText)
        private val advInterval: TextView = view.findViewById(R.id.advInterval)
        private val timestamp: TextView = view.findViewById(R.id.timestamp)

        private val detailsButton: Button = view.findViewById(R.id.detailsButton)
        private val bondButton: Button = view.findViewById(R.id.bondButton)

        private lateinit var scanItem: ScanItem

        fun bind(position: Int) {
            scanItem = ScanItem(scanItems[position])

            setDeviceName()
            setDeviceAddress()
            setBondState()
            setConnectableStatus()
            setRssi()
            setAdvInterval()
            setTimestamp()

            detailsButton.setOnClickListener { onClickListener.showDetails(scanItem) }
            bondButton.setOnClickListener { onClickListener.bond(scanItem) }
        }

        private fun setDeviceName() {
            deviceName.text = if (scanItem.name.isEmpty()) "Unknown" else scanItem.name
        }

        private fun setDeviceAddress() {
            deviceAddress.text = scanItem.address
        }

        private fun setBondState() {
            when(scanItem.bondState) {
                BluetoothDevice.BOND_BONDED -> {
                    bondState.text = context.resources.getString(R.string.bonded_text)
                    bondButton.visibility = View.GONE
                }
                BluetoothDevice.BOND_BONDING -> {
                    bondState.text = context.resources.getString(R.string.bonding_text)
                    bondButton.visibility = View.GONE
                }
                else -> {
                    bondState.text = context.resources.getString(R.string.not_bonded_text)
                    bondButton.visibility = View.VISIBLE
                }
            }
        }

        private fun setConnectableStatus() {
            isConnectable.text =
                if (scanItem.isConnectable) context.resources.getString(R.string.yes)
                else context.resources.getString(R.string.no)
        }

        private fun setRssi() {
            rssi.text = if (scanItem.rssi > -128) scanItem.rssi.toString() + " dBm" else "-"
        }

        private fun setAdvInterval() {
            advInterval.text = if (scanItem.advInterval == 0) "Unknown" else scanItem.advInterval.toString() + " ms"
        }

        private fun setTimestamp() {
            timestamp.text = if (scanItem.datetime.isEmpty()) "Unknown" else scanItem.datetime
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.scan_list_item, parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(position)

    override fun getItemCount() = scanItems.count()

    fun setScanItems(scanItems: ArrayList<ScanItem>, index: Int) {
        if (multipleItemsUpdated(scanItems)) {
            updateMultipleItems(scanItems)
            return
        }

        if (scanItems.isEmpty()) {
            clearList()
            return
        }

        if (newListItem(index)) {
            appendNewItem(scanItems, index)
            return
        }

        updateItem(scanItems, index)
    }

    private fun multipleItemsUpdated(scanItems: ArrayList<ScanItem>): Boolean {
        return this.scanItems.isEmpty() && scanItems.count() > 1
    }

    private fun updateMultipleItems(scanItems: ArrayList<ScanItem>) {
        this.scanItems = scanItems.toMutableList()
        notifyDataSetChanged()
    }

    private fun clearList() {
        this.scanItems.clear()
        notifyDataSetChanged()
    }

    private fun newListItem(index: Int) = this.scanItems.count() <= index

    private fun appendNewItem(scanItems: ArrayList<ScanItem>, index: Int) {
        this.scanItems.add(ScanItem(scanItems[index]))
        notifyItemInserted(index)
    }

    private fun updateItem(scanItems: ArrayList<ScanItem>, index: Int) {
        this.scanItems[index] = ScanItem(scanItems[index])
        notifyItemChanged(index)
    }
}