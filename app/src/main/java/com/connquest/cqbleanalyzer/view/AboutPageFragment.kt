package com.connquest.cqbleanalyzer.view

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.connquest.cqbleanalyzer.R
import com.connquest.cqbleanalyzer.databinding.AboutPageBinding


class AboutPageFragment: Fragment() {
    private lateinit var viewModel: AboutPageViewModel
    private lateinit var binding: AboutPageBinding

    private lateinit var eventListener: EventListener

    interface EventListener {
        fun aboutPageCreated()
    }

    fun openInstagramPage(view: View) {
        val uri = Uri.parse(viewModel.instagramPage)
        openPage(uri)
    }

    fun openLinkedInPage(view: View) {
        val uri = Uri.parse(viewModel.linkedInPage)
        openPage(uri)
    }

    private fun openPage(uri: Uri) {
        val intent = Intent(Intent.ACTION_VIEW, uri)
        startActivity(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        menu.findItem(R.id.action_scan).isVisible = false
        menu.findItem(R.id.action_stop_scan).isVisible = false
        super.onPrepareOptionsMenu(menu)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        eventListener = context as EventListener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.about_page, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        eventListener.aboutPageCreated()

        viewModel = ViewModelProvider(requireActivity()).get(AboutPageViewModel::class.java)

        binding.lifecycleOwner = this
        binding.fragment = this
        binding.model = viewModel
    }
}