package com.connquest.cqbleanalyzer.view

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class AboutPageViewModel: ViewModel() {
    private val appVersion = MutableLiveData<String>()
    private val domain = MutableLiveData<String>()

    fun appVersion(): LiveData<String> = appVersion
    fun domain(): LiveData<String> = domain

    private val appVersionString = "1.0"
    private val domainString = "http://connquest.eu/"

    val instagramPage = "https://instagram.com/connectionquest?igshid=1jpsjrqlp9q3"
    val linkedInPage = "https://www.linkedin.com/company/connection-quest/"

    init {
        appVersion.value = appVersionString
        domain.value = domainString
    }
}