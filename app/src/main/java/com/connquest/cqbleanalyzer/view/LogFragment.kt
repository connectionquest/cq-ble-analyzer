package com.connquest.cqbleanalyzer.view

import android.content.Context
import android.content.res.Resources
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.format.DateFormat
import android.text.style.ForegroundColorSpan
import android.util.TypedValue
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.connquest.cqbleanalyzer.R
import com.connquest.cqbleanalyzer.logger.LogMessage
import com.connquest.cqbleanalyzer.logger.MessagePriority
import java.util.*


class LogFragment: Fragment() {
    private lateinit var viewModel: LogViewModel
    private lateinit var eventListener: EventListener

    private lateinit var log: TextView
    private lateinit var scrollView: ScrollView

    private val calendar = Calendar.getInstance()

    interface EventListener {
        fun logFragmentCreated()
        fun exportLog(logMessages: List<LogMessage>)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.log_menu, menu)

        setupPrioritySpinner(menu)
        setupTimestampVisibility(menu)

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.time_checkbox -> resolveTimestampVisibility(item)
            R.id.action_export -> eventListener.exportLog(viewModel.logMessages().value!!.filter {
                it.priority.value >= viewModel.messagePriority.value })
            R.id.action_clear -> viewModel.clearLog()
        }
        return false
    }

    private fun resolveTimestampVisibility(item: MenuItem) {
        viewModel.isTimestampVisible = !viewModel.isTimestampVisible

        if (viewModel.isTimestampVisible) item.title = requireContext().resources.getString(R.string.hide_time)
        else item.title = requireContext().resources.getString(R.string.show_time)

        refreshLog()
    }

    private fun setupPrioritySpinner(menu: Menu) {
        val item = menu.findItem(R.id.action_log_priority)
        val spinner = item.actionView as Spinner

        ArrayAdapter.createFromResource(requireContext(), R.array.log_priority_names, R.layout.spinner_item)
            .also {
                    adapter -> adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinner.adapter = adapter
            }

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                viewModel.messagePriority =  when(p0?.getItemAtPosition(p2).toString()) {
                    "INFO" -> MessagePriority.INFO
                    "DEBUG" -> MessagePriority.DEBUG
                    "WARNING" -> MessagePriority.WARNING
                    "ERROR" -> MessagePriority.ERROR
                    else -> MessagePriority.INFO
                }
                refreshLog()
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {}
        }

        spinner.setSelection(viewModel.messagePriority.value)
    }

    private fun setupTimestampVisibility(menu: Menu) {
        val item = menu.findItem(R.id.time_checkbox)

        if (viewModel.isTimestampVisible) item.title = requireContext().resources.getString(R.string.hide_time)
        else item.title = requireContext().resources.getString(R.string.show_time)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        eventListener = context as EventListener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.log_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        eventListener.logFragmentCreated()

        log = requireActivity().findViewById(R.id.logTextView)
        scrollView = requireActivity().findViewById(R.id.logScrollView)

        viewModel = ViewModelProvider(requireActivity()).get(LogViewModel::class.java)
        viewModel.logMessages().observe(viewLifecycleOwner, Observer { updateLog(it) })
    }

    private fun refreshLog() {
        clearLog()
        updateLog(viewModel.logMessages().value!!)
    }

    private fun updateLog(logMessages: List<LogMessage>) {
        if (logMessages.isEmpty()) clearLog()
        else if (log.text.isEmpty()) appendAll(logMessages)
        else appendMessage(logMessages.last())
    }

    private fun appendAll(logMessages: List<LogMessage>) {
        for (logMessage in logMessages) appendMessage(logMessage)
    }

    private fun appendMessage(logMessage: LogMessage) {
        if (logMessage.priority.value < viewModel.messagePriority.value) return

        var timestamp = ""
        if (viewModel.isTimestampVisible) {
            calendar.timeInMillis = logMessage.timestamp
            timestamp = DateFormat.format("[HH:mm:ss]".padEnd(14), calendar).toString()
        }

        val message = timestamp + logMessage.message + "\n"
        val spannable = SpannableString(message)
        val textColor = getTextColor(logMessage.priority)
        spannable.setSpan(ForegroundColorSpan(textColor), 0, message.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        log.append(spannable)
        scrollView.post { scrollView.smoothScrollTo(0, log.bottom) }
    }

    private fun getTextColor(priority: MessagePriority): Int {
        return when (priority) {
            MessagePriority.INFO -> getInfoColor()
            MessagePriority.DEBUG -> getDebugColor()
            MessagePriority.WARNING -> getWarningColor()
            MessagePriority.ERROR -> getErrorColor()
        }
    }

    private fun getInfoColor(): Int {
        val theme: Resources.Theme = requireContext().theme
        val typedIndicatorColor = TypedValue()
        theme.resolveAttribute(R.attr.logInfoColor, typedIndicatorColor, true)
        return typedIndicatorColor.data
    }

    private fun getDebugColor(): Int {
        val theme: Resources.Theme = requireContext().theme
        val typedBackgroundColor = TypedValue()
        theme.resolveAttribute(R.attr.logDebugColor, typedBackgroundColor, true)
        return typedBackgroundColor.data
    }

    private fun getWarningColor(): Int {
        val theme: Resources.Theme = requireContext().theme
        val typedIndicatorColor = TypedValue()
        theme.resolveAttribute(R.attr.logWarningColor, typedIndicatorColor, true)
        return typedIndicatorColor.data
    }

    private fun getErrorColor(): Int {
        val theme: Resources.Theme = requireContext().theme
        val typedBackgroundColor = TypedValue()
        theme.resolveAttribute(R.attr.logErrorColor, typedBackgroundColor, true)
        return typedBackgroundColor.data
    }

    private fun clearLog() {
        log.text = ""
    }
}