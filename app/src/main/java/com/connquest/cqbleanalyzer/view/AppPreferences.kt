package com.connquest.cqbleanalyzer.view

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Resources
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.annotation.ColorInt
import androidx.preference.CheckBoxPreference
import androidx.preference.ListPreference
import androidx.preference.PreferenceFragmentCompat
import com.connquest.cqbleanalyzer.R


class AppPreferences: PreferenceFragmentCompat(), SharedPreferences.OnSharedPreferenceChangeListener {
    private lateinit var eventListener: EventListener

    interface EventListener {
        fun preferencesOpened()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        eventListener = context as EventListener
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        menu.clear()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        @ColorInt val backgroundColor = resolveBackgroundColor()
        view?.setBackgroundColor(backgroundColor)

        eventListener.preferencesOpened()
        return view
    }

    private fun resolveBackgroundColor(): Int {
        val theme: Resources.Theme = requireContext().theme
        val typedBackgroundColor = TypedValue()
        theme.resolveAttribute(R.attr.preferencesBackground, typedBackgroundColor, true)
        return typedBackgroundColor.data
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)

        val autoscanPreference: CheckBoxPreference? = findPreference(resources.getString(R.string.autoscanKey))
        changeAutoscanValueVisibility(autoscanPreference!!.isChecked)
    }

    private fun changeAutoscanValueVisibility(isVisible: Boolean) {
        val autoscanValuePreference: ListPreference? = findPreference(resources.getString(R.string.autoscanValueKey))
        autoscanValuePreference?.isVisible = isVisible
    }

    override fun onSharedPreferenceChanged(p0: SharedPreferences?, p1: String?) {
        p0 ?: return
        p1 ?: return

        if (p1 == resources.getString(R.string.autoscanKey)) changeAutoscanValueVisibility(p0.getBoolean(p1, false))
    }

    override fun onResume() {
        super.onResume()
        preferenceManager.sharedPreferences.registerOnSharedPreferenceChangeListener(this)
    }

    override fun onPause() {
        preferenceManager.sharedPreferences.unregisterOnSharedPreferenceChangeListener(this)
        super.onPause()
    }
}