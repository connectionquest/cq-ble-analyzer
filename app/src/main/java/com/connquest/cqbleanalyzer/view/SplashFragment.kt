package com.connquest.cqbleanalyzer.view

import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.VideoView
import androidx.fragment.app.Fragment
import com.connquest.cqbleanalyzer.R


class SplashFragment: Fragment() {
    private lateinit var eventListener: EventListener

    private lateinit var videoView: VideoView

    interface EventListener {
        fun splashFragmentCreated()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        eventListener = context as EventListener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.splash_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        eventListener.splashFragmentCreated()

        videoView = requireActivity().findViewById(R.id.splashVideo)
        val path = "android.resource://" + requireActivity().packageName + "/" + R.raw.splash
        videoView.setVideoURI(Uri.parse(path))
        videoView.setBackgroundColor(Color.BLACK)
        videoView.postDelayed({ videoView.setBackgroundColor(Color.TRANSPARENT) }, 300)
        videoView.start()
    }

    override fun onDestroy() {
        videoView.stopPlayback()
        super.onDestroy()
    }
}