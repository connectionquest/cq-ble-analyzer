package com.connquest.cqbleanalyzer.view

import android.bluetooth.BluetoothDevice
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.connquest.cqbleanalyzer.scan.ScanItem

class DeviceDetailsViewModel: ViewModel() {
    private var deviceCache: MutableSet<ScanItem> = mutableSetOf()
    private var deviceShown = MutableLiveData<ScanItem>()
    private var deviceName: String = "Unknown"
    private var deviceInCache: ScanItem = ScanItem()

    init {
        deviceShown.value = ScanItem()
    }

    fun device(): LiveData<ScanItem> = deviceShown

    fun setDevice(scanItem: ScanItem) {
        if (isDeviceCached(scanItem)) scanItem.services = LinkedHashMap(deviceInCache.services)

        deviceName = if (scanItem.name.isEmpty()) "Unknown" else scanItem.name
        deviceShown.postValue(scanItem)
    }

    fun isDeviceCached(scanItem: ScanItem): Boolean {
        for (item in deviceCache) {
            if (scanItem.address == item.address) {
                deviceInCache = item
                return true
            }
        }
        return false
    }

    fun getDeviceName() = deviceName

    fun updateDeviceIfShown(scanItem: ScanItem) {
        if (isDeviceShown(scanItem.address)) {
            val item = ScanItem(deviceShown.value)

            item.name = scanItem.name
            item.bondState = scanItem.bondState
            item.isConnectable = scanItem.isConnectable
            item.deviceType = scanItem.deviceType
            item.batteryLevel = scanItem.batteryLevel
            item.updateRssi(scanItem.rssi)
            item.txPower = scanItem.txPower
            item.datetime = scanItem.datetime
            item.updateAdvInterval(scanItem.timestamp)
            item.primaryPhy = scanItem.primaryPhy
            item.secondaryPhy = scanItem.secondaryPhy
            item.dataStatus = scanItem.dataStatus
            item.rawData = scanItem.rawData

            setDevice(item)
        }
    }

    private fun isDeviceShown(address: String) = (address == deviceShown.value?.address)

    fun servicesDiscovered(scanItem: ScanItem) {
        if (!isDeviceCached(scanItem)) {
            deviceCache.add(scanItem)
        }
        if (isDeviceShown(scanItem.address)) {
            deviceShown.value?.services = LinkedHashMap(scanItem.services)
            deviceShown.postValue(deviceShown.value)
        }
    }

    fun bondStateChanged(device: BluetoothDevice) {
        if (isDeviceShown(device.address)) {
            deviceShown.value?.bondState = device.bondState
            if (deviceShown.value != null) setDevice(deviceShown.value!!)
        }
    }
}