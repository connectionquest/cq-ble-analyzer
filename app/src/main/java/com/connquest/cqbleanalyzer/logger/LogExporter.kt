package com.connquest.cqbleanalyzer.logger

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.text.format.DateFormat
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat.startActivityForResult
import com.connquest.cqbleanalyzer.utility.Request
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.lang.NullPointerException
import java.util.*

class LogExporter(private var eventListener: EventListener) {
    private var context = eventListener as Context
    private var contentResolver = context.contentResolver

    private val calendar = Calendar.getInstance()
    private lateinit var logMessages: List<LogMessage>

    interface EventListener {
        fun exportSuccessful()
        fun exportFailed()
    }

    fun setContext(eventListener: EventListener) {
        this.eventListener = eventListener
        context = eventListener as Context
        contentResolver = context.contentResolver
    }

    fun export(logMessages: List<LogMessage>) {
        this.logMessages = logMessages
        calendar.timeInMillis = logMessages.last().timestamp
        createFile()
    }

    private fun createFile() {
        val timestamp = DateFormat.format("[dd-MM-yyyy HH:mm:ss]    ", calendar).toString()
        val intent = Intent(Intent.ACTION_CREATE_DOCUMENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "text/plain"
            putExtra(Intent.EXTRA_TITLE, "${timestamp.trim()}.txt")
        }
        startActivityForResult(context as AppCompatActivity, intent, Request.createFile, null)
    }

    fun writeToFile(uri: Uri?) {
        try {
            GlobalScope.launch {
                val content = parseLogMessages()
                contentResolver.openFileDescriptor(uri!!, "w")?.use {
                    FileOutputStream(it.fileDescriptor).use {
                        it.write(content.toByteArray())
                    }
                }
                eventListener.exportSuccessful()
            }
        } catch (e: NullPointerException) {
            eventListener.exportFailed()
        } catch (e: FileNotFoundException) {
            eventListener.exportFailed()
        } catch (e: IOException) {
            eventListener.exportFailed()
        }
    }

    private fun parseLogMessages(): String {
        var content = ""
        for (logMessage in logMessages) content += parseMessage(logMessage)
        return content
    }

    private fun parseMessage(logMessage: LogMessage): String {
        val priority = "${logMessage.priority}".padEnd(11)

        calendar.timeInMillis = logMessage.timestamp
        val timestamp = DateFormat.format("[dd-MM-yyyy HH:mm:ss]".padEnd(24), calendar).toString()

        return priority + timestamp + logMessage.message + "\n"
    }
}