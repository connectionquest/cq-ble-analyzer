package com.connquest.cqbleanalyzer.logger

data class LogMessage(val priority: MessagePriority, val timestamp: Long, val message: String) {}