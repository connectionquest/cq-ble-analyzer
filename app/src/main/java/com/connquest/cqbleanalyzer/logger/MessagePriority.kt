package com.connquest.cqbleanalyzer.logger

enum class MessagePriority(val value: Int) {
    INFO(0), DEBUG(1), WARNING(2), ERROR(3);

    companion object {
        fun getValueForOrdinal(ordinal: Int): MessagePriority = values()[ordinal]
    }
}